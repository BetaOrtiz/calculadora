/*Mockar os metódos necesários
*/
document = {
    querySelector : function() { return {} },
    querySelectorAll : function() { return [] },
    addEventListener : function() {}
}

const CalcController = require('./CalcController')
let calc = new CalcController()

/* Test1*/
console.log("Teste se ao digitar um número o display funciona corretamente")

/*Ação*/
calc.addOperation(1)
/*Resultado esperado*/
assertEquals(calc.displayCalc, 1)

/*Test2*/
console.log("Teste se ao digitar outro número o display funciona corretamente")

// Ação
calc.addOperation(2)
// Resultado esperado
assertEquals(calc.displayCalc, 12)

/*Test3*/
console.log("Teste se limpar o display tá funcionando corretamente")

// Ação - 
calc.clearAll()
// Resultado esperado
assertEquals(calc.displayCalc, 0)

function assertEquals(value, expect) {
    let assert = value == expect

    if (!assert) {
        console.log("Teste falhou")
        process.exit(1)
    } else {
        console.log("Teste passou")
    }
}
process.exit(0)